import kotlin.math.max
import kotlin.math.min

fun main() {
    data.filter {
        it.dlc != null
    }.forEach { block ->
        with(block) {
            val interchangeables = data.filter {
                it.dlc == null &&
                it.gridSize == block.gridSize &&
                it.typeId == block.typeId &&
                it.xsiType == block.xsiType &&
                it.pcu == block.pcu &&
                it.sizeToSet() == block.sizeToSet() &&
                block.humanName.compareByWordSequences(it.humanName) > 0.0001
            }

            if (interchangeables.size < 10) {
                println("\n$typeId/$xsiType/$subtypeId")
                interchangeables.forEach {
                    println("\t${it.typeId}/${it.xsiType}/${it.subtypeId}")
                }
            }
        }
    }
}

fun BlockData.sizeToSet(): Set<Int>? {
    return blockSize?.let {
        setOf(it.x, it.y, it.z)
    }
}

/**maybe x10 every time we get another identical word in a row?
 * +1 for an identical word, x10 for each sequential identical word.
 *
 * But this algo would give longer sequences more chances to rack up a high score than shorter sequence.
 * So we need to divide by number of words*/
/*fun String.compareByWordRuns(other: String): Long {
    val words = this.split(" ")
    val otherWords = other.split(" ")
    var highestSequenceOfIdenticalWords = 0
    words.forEachIndexed { i, word ->
        otherWords.forEach { otherWord ->
            if(word == otherWord) {

            }
        }
    }
}*/
/*
len(upper) = 7
len(lower) = 6

           a b c d e f g
 i j k l a b
 offset = -5
 overlap = 1
 upper start = 0
 lower start = 5
 upper end   = 0
 lower end   = 5


         a b c d e f g
 i j k l a b
 offset = -4
 overlap = 2
 upper start = 0
 lower start = 4
 upper end   = 1
 lower end   = 5


   a b c d e f g
 i j k l a b
 offset = -1
 overlap = 5
 upper start = 0
 lower start = 1
 upper end   = 4
 lower end   = 5


 a b c d e f g
 i j k l a b
 offset = 0
 overlap = 6
 upper start = 0
 lower start = 0
 upper end   = 5
 lower end   = 5


 a b c d e f g
   i j k l a b
 offset = 1
 overlap = 6
 upper start = 1
 lower start = 0
 upper end   = 6
 lower end   = 5


a b c d e f g
          i j k l a b
 offset = 5
 overlap = 2
 upper start = 5
 lower start = 0
 upper end   = 6
 lower end   = 1


 a b c d e f g
             i j k l a b
 offset = 6
 overlap = 1
 upper start = 6
 lower start = 0
 upper end   = 6
 lower end   = 0

 -------------------------------------------------------------------------------------
len(upper) = 5
len(lower) = 7

             a b c d e
 l m n o p q r
 offset = -6
 overlap = 1
 upper start = 0
 lower start = 6
 upper end   = 0
 lower end   = 6


            a b c d e
 l m n o p q r
 offset = -5
 overlap = 2
 upper start = 0
 lower start = 5
 upper end   = 1
 lower end   = 6


     a b c d e
 l m n o p q r
 offset = -2
 overlap = 5
 upper start = 0
 lower start = 2
 upper end   = 4
 lower end   = 6


   a b c d e
 l m n o p q r
 offset = -1
 overlap = 5
 upper start = 0
 lower start = 1
 upper end   = 4
 lower end   = 5


 a b c d e
 l m n o p q r
 offset = 0
 overlap = 5
 upper start = 0
 lower start = 0
 upper end   = 4
 lower end   = 4


 a b c d e
   l m n o p q r
 offset = 1
 overlap = 4
 upper start = 1
 lower start = 0
 upper end   = 4
 lower end   = 3


 a b c d e
       l m n o p q r
 offset = 3
 overlap = 2
 upper start = 3
 lower start = 0
 upper end   = 4
 lower end   = 1


 a b c d e
         l m n o p q r
 offset = 4
 overlap = 1
 upper start = 4
 lower start = 0
 upper end   = 4
 lower end   = 0



so observations from the above examples ignoring what I think I worked out below):
highest and lowest offsets between the sequences, with any overlap
val lastUpperIndex = len(upper)-1
val lastLowerIndex = len(lower)-1
lowest offset = 0 - lastLowerIndex
    so lower length of 6 = -5

highest offset = lastUpperIndex

upper start = max(0, offset)
lower start = max(0, 0-offset)
upper end   = min(offset + lastLowerIndex, lastUpperIndex )
lower end   = min( lastLowerIndex, lastUpperIndex - offset )


*/


//comparing two lists of strings
// in the examples, the upper is the this, the lower is the other
// every possible comparison for longest identical sequence can be described by a scalar offset value
// from negative the length of the lower string -1 (for a lower list of length 6 = -5)
// to positive  the length of the upper string minus one (for an upper list of length 7 = 6)
// total positions = 12
// ( sum of both lengths +1?)


// so for each offset, compare whether each pair of words match in a list<Boolean>
/**Compares two strings by the longest run of identical words in `other`, divided by the total number of words in `this`.
 * @return a double between 0 and 1. 0 means no words in common,
 * 1 means every single word is identical and appears in the same order.
 * Note that 1 is also returned when `other` is a strict superset of `this`;
 * i.e, when `other.contains(this) == true`.*/
fun String.compareByWordSequences(other: String): Double {
    //store each word check in a List<List<Boolean>>
    //the inner List<Boolean> stores whether each word in the overlap for that offset was identical or not
    //outer list length is the number of offset positions
    // inner list length is each offset's overlap length (each one is different)
    // the outer list is of every possible offset with any overlap
    val comparisons:MutableList<MutableList<Boolean>> = mutableListOf()
    val upper = this.split(" ")
    val lower = other.split(" ")

    //lowest offset = 0 - lastLowerIndex
    val lowestOffset = (0-lower.lastIndex)

    //highest offset = lastUpperIndex
    val highestOffset = upper.lastIndex

    for( offset in lowestOffset..highestOffset) {
        //the index to use in the outer list for this offset run
        //so I need to be able to convert offset to/from negative values,
        //by adding some quantity that makes the lowest offset 0
        // can't I just do offset - lowestOffset?
        // if lowest offset is -5, then when offset is -5, then it's -5 - -5 = -5 + 5

        val offsetIndex = offset - lowestOffset

        //upper start = max(0, offset)
        val upperStart = max(0, offset)

        //lower start = max(0, 0-offset)
        val lowerStart = max(0, 0-offset)

        //upper end   = min(offset + lastLowerIndex, lastUpperIndex )
        val upperEnd = min(offset + lower.lastIndex, upper.lastIndex)

        //lower end   = min( lastLowerIndex, lastUpperIndex - offset )
        val lowerEnd = min(upper.lastIndex - offset, lower.lastIndex)

        var upperIndex = upperStart
        var lowerIndex = lowerStart

        comparisons.add(mutableListOf<Boolean>())
        while (true) {
            //store whether this pair of words matches
            comparisons.last().add(upper[upperIndex] == lower[lowerIndex])

            upperIndex++
            lowerIndex++
            if(upperIndex > upperEnd || lowerIndex > lowerEnd ){
                break
            }
        }
    }
    // then once we've done that,
    // create a mutable list of longest match = mutableListOf(0)
    val longestMatches = mutableListOf<Int>(0)
    // matches.forEach:
    comparisons.forEach { comparison ->
        comparison.forEachIndexed { i, match ->
            if(match == false) {
                // this match run is over!
                // if the last entry in the longestMatch string is >0:
                if(longestMatches.last() > 0) {
                    // append a new entry of 0
                    longestMatches.add(0)
                }
                // if the last entry is 0:
                // do nothing, move on (continue)
            } else {
                //increment the longest match in the last (current) run
                longestMatches[longestMatches.lastIndex]++
            }
        }
    }
    val longestOverallMatch = longestMatches.max()
    return longestOverallMatch.toDouble() / upper.size.toDouble()
}
